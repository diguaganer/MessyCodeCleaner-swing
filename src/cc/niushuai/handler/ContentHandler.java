package cc.niushuai.handler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.swing.JTextArea;

import org.apache.commons.io.IOUtils;

/** 
 * @author 牛帅
 * @desc 
 * @Email niushuai951101@gmail.com
 * @date 2018年8月9日 下午10:19:47 
*/
public class ContentHandler {

	/**
	 * 内容处理方法
	 * @param leftArea 左边区域
	 * @param rightArea 右边区域
	 * @throws IOException 
	 */
	public void run(JTextArea leftArea, JTextArea rightArea) throws IOException {
		
		byte[] leftBytes = getContent(leftArea);
		
		String content = getContentByBytes(leftBytes);
		
		rightArea.setText(content);
	}
	
	/**
	 * 字符数组转字符串
	 * @param leftBytes
	 * @return
	 * @throws IOException 
	 */
	private String getContentByBytes(byte[] bytes) throws IOException {
		
		if(null == bytes)
			return "";
		return IOUtils.toString(bytes, "UTF-8");
	}

	/**
	 * 得到文本域的内容字节数组
	 * @param area
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	private byte[] getContent(JTextArea area) throws UnsupportedEncodingException {
		return area.getText().getBytes("UTF-8");
	}
	
}
