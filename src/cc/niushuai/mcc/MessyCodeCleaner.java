/** 
 * @author 牛帅
 * @desc 主窗体类
 * @Email niushuai951101@gmail.com
 * @date 2018年8月9日 上午9:28:27 
 */

package cc.niushuai.mcc;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import cc.niushuai.handler.ContentHandler;

public class MessyCodeCleaner {

	private JFrame frame;// 主窗体
	private JScrollPane leftPanel;// 左边面板
	private JScrollPane rightPanel;// 右边面板
	private JTextArea leftArea;// 左边textarea区域
	private JTextArea rightArea;// 右边textarea区域
	private JButton btnClickMe;// 转换按钮

	/**
	 * 加载应用程序
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MessyCodeCleaner window = new MessyCodeCleaner();
					window.frame.setVisible(true);
					window.startTask();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void startTask(){
		//给按钮添加点击事件  采用匿名内部类的方式实现监听
		btnClickMe.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					new ContentHandler().run(leftArea,rightArea);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
	
	
	/**
	 * Create the application.
	 */
	public MessyCodeCleaner() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			// 初始化窗体
			frame = initFrame("注释乱码清除工具", 420, 220, 820, 620);

			leftPanel = new JScrollPane();// 初始化带有滚动条的面板

			leftArea = new JTextArea();// 初始化左边文本域
			leftPanel.setViewportView(leftArea);

			rightPanel = new JScrollPane();// 初始化右边面板

			rightArea = new JTextArea();// 初始化右边文本域
			// 使右边的框无法编辑，只能显示查看
			rightArea.setEditable(false);
			rightPanel.setViewportView(rightArea);

			btnClickMe = new JButton("Click me");// 初始化按鈕
			
			// 將内容裝入容器
			frame.getContentPane().add(leftPanel);
			frame.getContentPane().add(rightPanel);
			frame.getContentPane().add(btnClickMe);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	/**
	 * 
	 * @param title
	 *            窗体显示标题
	 * @param x1
	 *            距离屏幕左边的宽度
	 * @param y1
	 *            距离屏幕上边的高度
	 * @param x2
	 *            窗体的长度
	 * @param y2
	 *            窗体的宽度
	 * @return
	 */
	private JFrame initFrame(String title, int x1, int y1, int x2, int y2) {
		JFrame frame = new JFrame();
		frame.setTitle(title);
		frame.setBounds(x1, y1, x2, y2);
		// 默认关闭按钮有效
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// 设置布局方式
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.X_AXIS));
		return frame;
	}

}
