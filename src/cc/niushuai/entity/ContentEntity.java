package cc.niushuai.entity;

/**
 * @author 牛帅
 * @desc
 * @Email niushuai951101@gmail.com
 * @date 2018年8月9日 下午9:29:21
 */
public class ContentEntity {

	private Integer leftOrRightContent;//标记是左边框内容还是右边框内容
	private String content;//框内容
	
	public ContentEntity() {
		super();
	}
	
	public ContentEntity(Integer leftOrRightContent, String content) {
		super();
		this.leftOrRightContent = leftOrRightContent;
		this.content = content;
	}

	public Integer getLeftOrRightContent() {
		return leftOrRightContent;
	}

	public void setLeftOrRightContent(Integer leftOrRightContent) {
		this.leftOrRightContent = leftOrRightContent;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
